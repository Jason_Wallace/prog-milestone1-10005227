﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task29
{
    class Program
    {
        static void Main(string[] args)
        {

            {
                string words = " The quick brown fox jumped over the fence   ";
                char[] separator = new char[] { ' ' };
                int numberOfWords = words.Split(separator, StringSplitOptions.RemoveEmptyEntries).Length;
                Console.WriteLine("Number of words: {0}", numberOfWords);
            }
        }
    }
}