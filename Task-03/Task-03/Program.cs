﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose a conversion: 1 - Kilometres to Miles  2 - Miles to Kilometres");

            var menuSelection = Console.ReadLine();

            
                switch (menuSelection)
            {
                case "1":
                    Console.WriteLine("Convert Kilometres to Miles");
                    
                    {
                        var km = 0;
                        const double miles = 0.621371;

                        Console.WriteLine($"Enter a number to convert Kilometres to Miles");
                        km = int.Parse(Console.ReadLine());
                        var kmTomiles = miles * km;
                        Console.WriteLine($"O.K, so {kmTomiles} miles");
                        Console.WriteLine("Thank you");
                        break;
                    }

                case "2":
                    Console.WriteLine("Convert miles to km");


                    {
                        var miles = 0;
                            const double km = 1.60934;

                        Console.WriteLine($"Enter a number to convert Miles to Kilometres?");
                        miles = int.Parse(Console.ReadLine());
                        var milestokm = km * miles;
                        Console.WriteLine($"O.K, so {milestokm} kilometres");
                        Console.WriteLine("Thank you");
                        break;

                    }


                    
                default:
                    Console.WriteLine("Nothing was converted. Please restart the program to convert again");
                    break;
            }










        }



    }


        }
    

