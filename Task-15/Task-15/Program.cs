﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;

namespace Task
{ }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbersInput = new List<int>();

            Console.WriteLine("Please enter a number, or press enter when finished");
            string input = Console.ReadLine();


            while (input != "")
            {
                Console.WriteLine("Please enter another number, or press enter when finished: ");
                input = Console.ReadLine();
                int value;
                if (!int.TryParse(input, out value))
                {
                    // Error
                }
                else
                {
                    numbersInput.Add(value);
                }
            }
            int sum = 0;
            foreach (int value in numbersInput)
            {
                sum += value;
                Console.WriteLine("The number that was added to the list is : " + " " + value.ToString());
            }
        }
    }
