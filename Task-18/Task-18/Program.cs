﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new List<Tuple<string, double, string>>();
           people.Add(Tuple.Create("Mack", 1.00, "January"));
           people.Add(Tuple.Create("Jack", 2.00, "November"));
           people.Add(Tuple.Create("Beck", 4.00, "December"));
            foreach (var x in people)
            {
                Console.WriteLine($"This is {x.Item1}. He was born on {x.Item3} {x.Item2}");
            }


        }
    }
}
