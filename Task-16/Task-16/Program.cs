﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter a word");
            string input = Console.ReadLine();

            Console.WriteLine("Size of string: {0}", input.Length);

            
            string nameWithoutEmptyChar = input.Replace(" ", "");
            
            Console.WriteLine("Size of non empty character string: {0}",
            nameWithoutEmptyChar.Length);
            
            for (int counter = 0; counter <= nameWithoutEmptyChar.Length - 1; counter++)
                Console.WriteLine(nameWithoutEmptyChar[counter]);
            Console.ReadLine();
        }
    }
}

