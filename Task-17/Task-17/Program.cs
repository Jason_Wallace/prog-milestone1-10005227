﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string,
            int>>();
            names.Add(Tuple.Create("Jack", 32));
            names.Add(Tuple.Create("Bob", 31));
            names.Add(Tuple.Create("Joe", 87));

            foreach (var x in names)

                Console.WriteLine($"This person is {x.Item1} and he is {x.Item2}");
                

        }
    }
}
            