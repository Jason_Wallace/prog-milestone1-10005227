﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var age = 0;

            Console.WriteLine("What is your age?");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine("What is your name?");

            var name = (Console.ReadLine());
             
            Console.WriteLine($"Your name is " + name + " and you are " + age + " year(s) old.");
            Console.WriteLine("Your name is {0} and you are {1} year(s) old.", name, age);
            Console.WriteLine($"Your name is {name} and you are {age} year(s) old.");



        }
    }
}
