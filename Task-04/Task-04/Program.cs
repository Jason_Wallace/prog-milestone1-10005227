﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose a conversion: 1 - Celsius to Fahrenheit 2 - Fahrenheit to Celsius");

            var menuSelection = Console.ReadLine();


            switch (menuSelection)
            {
                case "1":
                    Console.WriteLine("Convert Celsius to Fahrenheit");

                    {
                     
                        Console.WriteLine($"Enter a number to convert Celsius to Fahrenheit");
                        int celsius = int.Parse(Console.ReadLine());
                        int ctof = ((celsius * 9) / 5) + 32;
                        Console.WriteLine("So the temperature in Fahrenheit is {0} Degrees", ctof);
                        Console.WriteLine("Thank you");
                        break;
                    }

                case "2":
                    Console.WriteLine("Convert Fahrenheit to Celsius");


                    {
                        Console.WriteLine($"Enter a number to convert Fahrenheit to Celsius");
                        int fahrenheit = int.Parse(Console.ReadLine());
                        int ftoc = ((fahrenheit - 32) / 9) * 5;
                        Console.WriteLine("So the temperature in Celsius is {0} Degrees", ftoc);
                        Console.WriteLine("Thank you");
                        break;


}



                default:
                    Console.WriteLine("Nothing was converted. Please restart the program to convert again");
                    break;
            }



        }
    }
}
