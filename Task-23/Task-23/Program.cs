﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                var ab = new Dictionary<string, string>();

                ab.Add("Monday", "weekday");
                ab.Add("Tuesday", "weekday");
                ab.Add("Wednesday", "weekday");
                ab.Add("Thursday", "weekday");
                ab.Add("Friday", "weekday");
                ab.Add("Saturday", "weekend");
                ab.Add("Sunday", "weekend");


           


                if (ab.ContainsKey("Monday"))
                {
                    Console.WriteLine($"Monday is a {ab["Monday"]}");
                }

                if (ab.ContainsKey("Tuesday"))
                {
                    Console.WriteLine($"Tuesday is a {ab["Tuesday"]}");
                }

                if (ab.ContainsKey("Wednesday"))
                {
                    Console.WriteLine($"Wednesday is a {ab["Wednesday"]}");

                    if (ab.ContainsKey("Thursday"))
                    {
                        Console.WriteLine($"Thursday is a {ab["Thursday"]}");

                        if (ab.ContainsKey("Friday"))
                        {
                            Console.WriteLine($"Friday is a {ab["Friday"]}");
                        }

                        if (ab.ContainsKey("Saturday"))
                        {
                            Console.WriteLine($"Saturday is a {ab["Saturday"]}");

                            if (ab.ContainsKey("Sunday"))
                            {
                                Console.WriteLine($"Sunday is a {ab["Sunday"]}");
                            }

                        }
                    }
                }
            }
        }
    }
}