﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            Console.WriteLine("Enter a year you want to check is a Leap year");
            year = int.Parse(Console.ReadLine());
            if (year % 400 == 0)
                Console.WriteLine("Year {0} is a Leap Year", year);
            else if (year % 100 == 0)
                Console.WriteLine("Year {0} is not a Leap Year", year);
            else if (year % 4 == 0)
                Console.WriteLine("Year {0} is a Leap Year", year);
            else
                Console.WriteLine("Year {0} is not a Leap Year", year);
            Console.ReadKey();
        }
    }
}

