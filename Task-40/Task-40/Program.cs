﻿using System;

namespace Task40
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime startDate = new DateTime(2016, 8, 1);
            DateTime endDate = new DateTime(2016, 8, 31);

            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            for (var i = 1; i <= days; i++)
            {
                var test = startDate.AddDays(i);
                switch (test.DayOfWeek)
                {
                    case DayOfWeek.Wednesday:
                        Console.WriteLine(test.ToShortDateString());
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}